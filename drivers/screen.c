#include "screen.h"
#include "ports.h"

int print_char(char c, int col, int row, char attr);
int get_cursor();
void set_cursor(int offset);
void clear_screen();
int get_screen_offset(int col, int row);
int get_offset_row(int offset);
int get_offset_col(int offset);


void print_at(char* msg, int col, int row) {
	int offset = 0;
	// Update the cursor if col and row not negative
	if(col >= 0 && row >= 0) offset = get_screen_offset(col, row);
	else {
		offset = get_cursor();
		row = get_offset_row(offset);
		col = get_offset_col(offset);
	}

	// Loop through each char of the msg and print it
	int i = 0;
	while(msg[i] != 0) {
	       	offset = print_char(msg[i++], col, row, WHITE_ON_BLACK);

		row = get_offset_row(offset);
		col = get_offset_col(offset);
	}
}

void print(char* msg) { print_at(msg, -1, -1); }

/*Print a char on the screen at col, row, or at cursor position*/
int print_char(char c, int col, int row, char attr) {
	/*Create a byte (char) pointer to the start of video memory*/
	unsigned char *vidmem = 0xb8000;

	/*If attribute byte is zero, assume the default style. */
	//if(!attr) attr = WHITE_ON_BLACK;
	
	/* Get the video memory offset for the screen location */
	int offset = 0;
	/* If col and row are non-negative, use them for offset. */
	//if(col >= 0 && row >= 0) offset = get_screen_offset(col, row);
	/* Otherwise , use the current cursor position . */
	//else offset = get_cursor();

	// If we see a newline character , set offset to the end of
	// current row , so it will be advanced to the first col
	// of the next row .
/*	if (c == '\n') {
		row = get_offset_row(offset);
		offset = get_screen_offset(79, row);
*/	// Otherwise, write the character and its attribute byte to
	// video memory at our calculated offset
	//}else {
		*(vidmem+offset) = 'P';
		//vidmem[offset+1] = attr;
//	}

	// Update the offset to the next character cell, which is
	// two bytes ahead of the current cell.
	//offset += 2;
	// Make scrolling adjustment, for when we reach the bottom
	// of the screen.
	//offset = handle_scrolling(offset);
	// Update the cursor position on the screen device.
	//set_cursor(offset);

	return offset;
}

int get_cursor() {
	// The device uses its control register as an index
	// to select its internal registers, of which we are
	// interested in:
	// 	reg 14: which is the high byte of the cursor's offset
	// 	reg 15: which is the low byte of the cursor's offset
	// Once the internal register has been selected, we may read or
	// write a byte on the data register.
	port_byte_out(REG_SCREEN_CTRL, 0x0E);
	int offset = port_byte_in(REG_SCREEN_DATA) << 8;
	port_byte_out(REG_SCREEN_CTRL, 0x0F);
	offset = port_byte_in(REG_SCREEN_DATA);
	// Since the cursor offset reported by the VGA hardware is the
	// number of characters, we multiply by two to convert it to
	// a character cell offset.
	return offset;
}

void set_cursor(int offset) {
	/* Like get_cursor, but writing not reading */
	offset /= 2; // char offset
	port_byte_out(REG_SCREEN_CTRL, 0x0E);
	port_byte_out(REG_SCREEN_DATA, (unsigned char)(offset >> 8));
	port_byte_out(REG_SCREEN_CTRL, 0x0F);
	port_byte_out(REG_SCREEN_DATA, (unsigned char)(offset & 0xFF));
}

void clear_screen() {
	int screen_size = MAX_COLS * MAX_ROWS;
	char *screen = VIDEO_ADDRESS;

	for(int i = 0; i < screen_size; i++) {
		screen[i*2] = '0';
	}
	set_cursor(0);
}

int get_screen_offset(int col, int row) { return (row * MAX_COLS + col) * 2; }
int get_offset_row(int offset) { return offset / (2 * MAX_COLS); }
int get_offset_col(int offset) { return (offset - (get_offset_row(offset) * 2 * MAX_COLS)/2); }
