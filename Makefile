# Automatically expand to a list of existing files that
# match the patterns
C_SOURCES = $(wildcard kernel/*.c drivers/*.c)
HEADERS = $(wildcard kernel/*.h drivers/*.h)

# Create a list of object files to build, simple by replacing
# the '.c' extension of filenames in C_SOURCES with '.o'
OBJ = ${C_SOURCES:.c=.o}

# Link kernel object files into one binary, making sure the
# entry code is right at the start of the binary.
all: os-image

run: all
	qemu-system-i386 -fda os-image

os-image: boot/bootloader.bin kernel.bin
	cat $^ > $@

# Build the kernel binary
kernel.bin: kernel/kernel_entry.o ${OBJ}
	ld -o $@ -Ttext 0x1000 $^ --oformat binary

# Build the kernel object file
%.o : %.c ${HEADERS}
	gcc -ffreestanding -c $< -o $@

# Build the kernel entry object file
%.o : %.asm
	nasm $< -f elf64 -o $@

%.bin: %.asm
	nasm $< -I 'boot/' -f bin -o $@

clean:
	rm -fr *.bin *.dis *.o os-image
	rm -fr kernel/*.o boot/*.bin drivers/*.o
