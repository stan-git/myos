[org 0x7c00]
	mov bp, 0x9000
	mov sp, bp

	mov bx, MSG_REAL_MODE
	call prints

	call switch_to_pm
	jmp $

%include "16bit/print/prints.asm"
%include "32bit/gdt.asm"
%include "32bit/print_pm.asm"
%include "32bit/switch_to_pm.asm"

[bits 32]
BEGIN_PM: 
	mov ebx, MSG_PROT_MODE
	call prints_pm

	jmp $

MSG_REAL_MODE db "Started in 16-bit real mode", 0
MSG_PROT_MODE db "Loaded 32-bit protected mode", 0

; bootsector
times 510-($-$$) db 0
dw 0xaa55
