;
; Print string function param(bx)
;


prints:
	pusha
	mov ah, 0x0e ; function number 0Eh : display char

loop_char:
	mov al, [bx]
	cmp al, 0
	je print_done
	int 0x10
	add bx, 1
	jmp loop_char


print_done: 
	popa
	ret

print_nl:
	pusha

	mov ah, 0x0e
	mov al, 0x0a	; newline char
	int 0x10
	mov al, 0x0d	; carriage return
	int 0x10

	popa
	ret
