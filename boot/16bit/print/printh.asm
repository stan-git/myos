;
; Function for printing Hex as String param(String dx)
;


printh:
	pusha
	mov cx, 4

loop_hex:
	sub cx, 1

	mov ax, dx
	shr dx, 4
	and ax, 0xf

	mov bx, HEX_OUT
        add bx, 2
        add bx, cx

	cmp al, 0xa

	jl set_letter
	add al, 0x7
	
	jmp set_letter	
	
set_letter:
	add al, 0x30

	mov [bx], al

	cmp cx, 0
	je printh_done
	jmp loop_hex

%include "16bit/print/prints.asm"

printh_done:
	mov bx, HEX_OUT
	call prints
	
	popa
	ret

HEX_OUT: db "0x0000",0
