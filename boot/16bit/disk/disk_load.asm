;
; Function to load sectors 'dh' from drive 'dl' into ES:BX
;

disk_load:
	pusha
	push dx

	mov ah, 0x02	; 0x02 = read mode
	mov al, dh	; dh = number of sectors to read
	mov cl, 0x02	; cl to choose a sector
			; 0x01 is the boot sector
	mov ch, 0x00	; cylinder
	mov dh, 0x00	; head

	int 0x13	; BIOS interrupt
	jc disk_error

	pop dx
	cmp al, dh	; check if we got all sectors
	jne sectors_error
	popa
	ret

disk_error:
	mov bx, DISK_ERROR
	call prints
	jmp $

sectors_error:
	mov bx, SECTORS_ERROR
	call prints

DISK_ERROR db "[!] Disk read error",0
SECTORS_ERROR db "[!] Incorrect number of sectors read",0
