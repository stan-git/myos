[org 0x7c00]

KERNEL_OFFSET equ 0x1000	; This is the memory offset to which we will load our kernel

	mov [BOOT_DRIVE], dl			; BIOS stores our boot drive in DL, so it's
				; best to remember this for later.
	mov bp, 0x9000		; Set-up the stack
	mov sp, bp

	mov bx, MSG_REAL_MODE	; Announce that we are starting 
	call prints		; booting from 16-bit real mode	
	call print_nl

	call load_kernel	; Load our kernel

	call switch_to_pm	; Switch to protected mode, from which
				; we will not return

	jmp $

%include "16bit/print/prints.asm"
%include "16bit/disk/disk_load.asm"
%include "32bit/gdt.asm"
%include "32bit/prints_pm.asm"
%include "32bit/switch_to_pm.asm"

[bits 16]

;load kernel
load_kernel:
	mov bx, MSG_LOAD_KERNEL	; Print a msg to say we are loading the kernel
	call prints
	call print_nl

	mov bx, KERNEL_OFFSET	; Set-up parameters for our diskload routine, so
	mov dh, 15		; that we load the first 15 sectors (excluding
	mov dl, [BOOT_DRIVE]	; the boot sector) from the boot disk (i.e. our
	call disk_load		; kernel code) to address KERNEL_OFFSET

	ret

[bits 32]
BEGIN_PM: 
	mov ebx, MSG_PROT_MODE
	call prints_pm
	
	call KERNEL_OFFSET	; Now jump to the address of our loaded
				; kernel code, assume the brace position

	jmp $

; Global variables
BOOT_DRIVE	db 0
MSG_REAL_MODE	db "Started in 16-bit real mode", 0
MSG_PROT_MODE	db "Loaded 32-bit protected mode", 0
MSG_LOAD_KERNEL	db "Loading kernel into memory.", 0

; bootsector
times 510-($-$$) db 0
dw 0xaa55
